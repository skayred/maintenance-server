class CreateProbes < ActiveRecord::Migration[5.0]
  def change
    create_table :probes do |t|
      t.integer :application_id
      t.string :name
      t.text :script

      t.timestamps
    end
  end
end
