class AddQualCharsAndAnalyzerToProbe < ActiveRecord::Migration[5.0]
  def change
    add_column :probes, :quality_characteristics, :text
    add_column :probes, :analyzer, :string
  end
end
