class AddQualityCharacteristicsToProbe < ActiveRecord::Migration[5.0]
  def change
    add_column :probes, :suitability, :float, default: 0
    add_column :probes, :performance, :float, default: 0
    add_column :probes, :compatibility, :float, default: 0
    add_column :probes, :usability, :float, default: 0
    add_column :probes, :reliability, :float, default: 0
    add_column :probes, :security, :float, default: 0
    add_column :probes, :maintainability, :float, default: 0
    add_column :probes, :portability, :float, default: 0
  end
end
