class CreateProbeInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :probe_infos do |t|
      t.integer :probe_id
      t.float :value
      t.float :normalized_value
      t.text :quality_characteristics
      t.text :params

      t.timestamps
    end
  end
end
