class AddQualityCharacteristicsToApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :suitability, :float, default: 0
    add_column :applications, :performance, :float, default: 0
    add_column :applications, :compatibility, :float, default: 0
    add_column :applications, :usability, :float, default: 0
    add_column :applications, :reliability, :float, default: 0
    add_column :applications, :security, :float, default: 0
    add_column :applications, :maintainability, :float, default: 0
    add_column :applications, :portability, :float, default: 0
  end
end
