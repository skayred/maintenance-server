namespace :npm do
  desc 'Runs rake sitemaps:generate'
  task :inst do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:stage) do
          execute :rake, 'npm:install'
        end
      end
    end
  end
end
