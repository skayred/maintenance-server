require 'gnuplot'
require 'moving_average'

require './linear_regression.rb'

ENV['RAILS_ENV'] = 'development'
DIR = File.dirname(__FILE__) 
require DIR + '/config/environment'

maintainability_indices = ProbeInfo.all.map(&:params).map do |p|
  {
    maintainability: p['maintainability'].to_f,
    datetime: DateTime.parse(p['datetime'])
  }
end
grouped = maintainability_indices.group_by_day { |p| p[:datetime] }

tmp_date = grouped.keys.first
daily = (grouped.keys.first..grouped.keys.last).map do |dat|
  if grouped[dat]
    tmp_date = dat
  end

  grouped[tmp_date].pluck(:maintainability).median
end

exp_avg = (9..daily.count-1).map { |i| daily.ema(i,10) }

reg = LinearRegression.new((0...5).to_a, exp_avg.last(5))

case
  when reg.slope == 0
    status = :ok
  when reg.slope > 0
    status = :good
  when reg.slope < 0
    status = :bad
end

result = {
  series: { exponential: exp_avg },
  status: status
}

p result
