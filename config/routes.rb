Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "registrations" }

  authenticated do
    root :to => 'applications#index', as: :authenticated
  end

  root :to => 'home#index'

  resources :home, only: [:index]
  resources :applications do
    resources :probes do
      resources :reports, only: [:index]
    end
    resources :reports, only: [:index]
  end
  resources :reports, only: [:index, :show]
  resource :user_accounts, only: [:show]
  namespace :api do
    resources :probe_infos
  end
end
