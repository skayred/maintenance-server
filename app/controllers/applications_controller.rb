class ApplicationsController < ApplicationController
  def index
    @applications = current_user.applications
  end

  def new
    @app = Application.new
  end

  def create
    @app = Application.new
    @app.name = params[:application][:name]
    @app.user = current_user

    @app.save!

    redirect_to applications_path
  end

  def destroy
    @app = Application.find(params[:id])
    @app.destroy if @app.user_id == current_user.id

    redirect_to applications_path
  end

  def edit
    @app = Application.find(params[:id])

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id
    render 'new'
  end

  def update
    @app = Application.find(params[:id])
    @app.name = params[:application][:name]
    @app.save! if @app.user_id == current_user.id

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id

    redirect_to applications_path(@app)
  end

  private
  def person_params
    params.require(:application).permit(:name)
  end
end
