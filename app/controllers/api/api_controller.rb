class Api::ApiController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!
  before_action :require_login

  alias_method :devise_current_user, :current_user

  def current_user
    authenticate_or_request_with_http_token do |token, options|
      User.find_by(token: token)
    end
  end

  def require_login
    unless current_user
      render status: 401, json: { error: 'Not logged in!' }
    end
  end
end
