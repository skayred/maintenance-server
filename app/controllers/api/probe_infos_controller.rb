class Api::ProbeInfosController < Api::ApiController
  before_action :check_application

  def create
    @probe_info = ProbeInfo.new
    @probe_info.probe_id = params[:probe_id]
    @probe_info.value = params[:value]
    @probe_info.params = params[:params].to_hash
    @probe_info.quality_characteristics = params[:quality_characteristics]

    unless params[:normalized_value]
      @probe_info.normalized_value = params[:normalized_value]
    end

    @probe_info.save!

    ProbesWorker.perform_async(@probe_info.id)

    render json: @probe_info
  end

  private
  def check_application
    unless current_user.id == Probe.find(params[:probe_id]).application.user_id
      render status: 401, json: { error: 'Wrong application!' }
    end
  end
end
