class ProbesController < ApplicationController
  def index
    @app = Application.find(params[:application_id])

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id
  end

  def new
    @app = Application.find(params[:application_id])
    @probe = Probe.new

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id
  end

  def edit
    @app = Application.find(params[:application_id])
    @probe = Probe.find(params[:id])

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id

    render 'new'
  end

  def create
    @app = Application.find(params[:application_id])

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id

    @probe = Probe.new
    @probe.application_id = @app.id
    @probe.name = params[:probe][:name]
    @probe.script = params[:probe][:script]
    @probe.analyzer = params[:analyzer]
    @probe.quality_characteristics = params[:quals].split(',')
    @probe.save!

    redirect_to application_probes_path(@app)
  end

  def update
    @app = Application.find(params[:application_id])

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id

    @probe = Probe.find(params[:id])
    @probe.application_id = @app.id
    @probe.name = params[:probe][:name]
    @probe.script = params[:probe][:script]
    @probe.analyzer = params[:analyzer]
    @probe.quality_characteristics = params[:quals].split(',')
    @probe.save!

    redirect_to application_probes_path(@app)
  end

  def destroy
    @app = Application.find(params[:application_id])
    
    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id

    @probe = Probe.find(params[:id])
    @probe.destroy

    redirect_to application_probes_path(@app)
  end
end
