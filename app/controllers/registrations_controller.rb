class RegistrationsController < Devise::RegistrationsController
  def create
    super do
      uname = params[:user][:email]
      pw = params[:user][:password]
      pwd = pw.crypt("$5$a1")

      UserAccountWorker.perform_async(uname, pwd)
    end
  end
end
