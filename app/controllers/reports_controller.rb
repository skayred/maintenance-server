class ReportsController < ApplicationController
  def index
    @page = params[:page] || 1

    if params[:probe_id]
      app = Probe.find(params[:probe_id]).application

      raise ActionController::RoutingError.new('Not Found') unless app.user_id == current_user.id

      results = Probe.find(params[:probe_id]).results.order_by(created_at: :desc).page(@page)
    elsif params[:application_id]
      app = Application.find(params[:application_id])

      raise ActionController::RoutingError.new('Not Found') unless app.user_id == current_user.id

      results = Application.find(params[:application_id]).results.order_by(created_at: :desc).page(@page)
    else
      results = Result.where(:application_id.in => current_user.applications.pluck(:id)).order_by(created_at: :desc).page(@page)
    end

    @reports = results
  end

  def show
    @report = Result.find(params[:id])
    @app = @report.application

    raise ActionController::RoutingError.new('Not Found') unless @app.user_id == current_user.id
  end
end
