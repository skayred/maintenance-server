class Probe < ActiveRecord::Base
  belongs_to :application
  has_many :probe_infos
  has_many_documents :results

  serialize :quality_characteristics, JSON

  # after_update :recalculate_probe_infos

  def recalculate_probe_infos
    probe_infos.each do |probe_info|
      ProbesWorker.perform_async(probe_info.id)
    end
  end

  def results_danger?
    results.select { |r| r.status == :bad }.count > 0
  end
end
