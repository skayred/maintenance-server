class ProbeInfo < ActiveRecord::Base
  belongs_to :probe

  serialize :params, JSON
end
