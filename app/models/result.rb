class Result
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::ActiveRecordBridge

  after_create :update_characteristics

  belongs_to_record :application
  belongs_to_record :probe

  field :application_id, type: Integer
  field :probe_id, type: Integer
  field :status, type: Symbol
  field :series, type: Hash
  field :problematic_modules, type: Array

  def update_characteristics
    application.update_characteristics
  end
end