class Application < ActiveRecord::Base
  belongs_to :user
  has_many :probes
  has_many_documents :results

  def update_characteristics
    qual_statuses = {
      bad: 0.0,
      ok: 0.5,
      good: 1.0
    }

    qual_percentage = {}
    probes.each do |p|
      if p.quality_characteristics
        probe_chars = {}
        p.quality_characteristics.each do |q|
          result = p.results.last
          if qual_percentage[q]
            qual_percentage[q] << (result.nil? ? :bad : result.status)
          else
            qual_percentage[q] = [result.nil? ? :bad : result.status]
          end
          probe_chars[q] = [result.nil? ? :bad : result.status]
        end
        p.update_attributes(Hash[probe_chars.map { |k,v| [ k, v.inject(0) { |sum,status| sum + qual_statuses[status] }/v.count.to_f ] }])
      end
    end

    update_attributes(Hash[qual_percentage.map { |k,v| [ k, v.inject(0) { |sum,status| sum + qual_statuses[status] }/v.count.to_f ] }])
  end

  def results_danger?
    results.select { |r| r.status == :bad }.count > 0
  end
end
