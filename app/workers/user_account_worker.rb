class UserAccountWorker
  include Sidekiq::Worker

  def perform(name, password_hash)
    # system("sudo useradd -m -p '#{ password_hash }' #{ name }")

    account_name = "\"#{name}\""
    view_name = name.gsub(/\W/, '')

    ActiveRecord::Base.connection.execute("CREATE USER #{account_name};")
    ActiveRecord::Base.connection.execute("GRANT CONNECT ON DATABASE main TO #{account_name};")
    ActiveRecord::Base.connection.execute("GRANT USAGE ON SCHEMA public TO #{account_name};")

    view_sql = 'SELECT'\
                  ' applications.id app_id,'\
                  ' applications.name app_name,'\
                  ' probes.id probe_id,'\
                  ' probes.name probe_name,'\
                  ' probe_infos.id probe_info_id,'\
                  ' probe_infos.value,'\
                  ' probe_infos.normalized_value,'\
                  ' probe_infos.quality_characteristics,'\
                  ' probe_infos.params,'\
                  ' probe_infos.created_at,'\
                  ' probe_infos.updated_at'\
                ' FROM'\
                  ' users'\
                  ' JOIN'\
                    ' applications ON applications.user_id = users.id'\
                  ' JOIN'\
                    ' probes ON applications.id = probes.application_id'\
                  ' JOIN'\
                    ' probe_infos ON probe_infos.probe_id = probes.id'\
                " WHERE users.email = '#{name}';"

    ActiveRecord::Base.connection.execute("CREATE VIEW probe_infos_user_#{view_name} AS #{view_sql}")
    ActiveRecord::Base.connection.execute("GRANT SELECT ON probe_infos_user_#{view_name} TO #{account_name}")
  end
end
