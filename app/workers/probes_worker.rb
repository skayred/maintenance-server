require 'v8'

class ProbesWorker
  include Sidekiq::Worker

  def perform(probe_info_id)
    probe_info = ProbeInfo.find(probe_info_id)
    probe = probe_info.probe

    if probe.try(:script)
      cxt = V8::Context.new
      cxt['value'] = probe_info.value
      probe_info.normalized_value = cxt.eval(probe.script)
    else
      probe_info.normalized_value = probe_info.value
    end

    probe_info.save!

    analyzer_class = Object.const_get("Analyzers::#{probe.analyzer}")
    result = analyzer_class.new(probe.id).calculate
    Result.create(result)
  end
end
