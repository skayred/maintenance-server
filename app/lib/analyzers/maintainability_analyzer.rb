require 'moving_average'
require './app/lib/analyzers/linear_regression.rb'

module Analyzers
  class MaintainabilityAnalyzer
    def initialize(probe_id)
      @probe = Probe.find(probe_id)
    end

    def calculate
      exp_length = 5
      maintainability_indices = @probe.probe_infos.map(&:params).map do |p|
        {
          maintainability: p['maintainability'].to_f,
          datetime: DateTime.parse(p['datetime'])
        }
      end
      grouped = maintainability_indices.group_by_day { |p| p[:datetime] }

      tmp_date = grouped.keys.first
      dates = {}
      medians = {}
      daily = (grouped.keys.first..grouped.keys.last).map.with_index do |dat,i|
        if grouped[dat]
          tmp_date = dat
        end

        dates[i] = tmp_date
        medians[tmp_date] = grouped[tmp_date].pluck(:maintainability).median
      end

      modules = []
      #modules = @probe.probe_infos.last.params['modules'].select { |m| m['maintainability'].to_f <= 60 }

      exp_avg = ((exp_length-1)..medians.count-1).map { |i| [ medians.keys[i], medians.values.ema(i,exp_length) ] }

      if exp_avg.count > 5
        reg = LinearRegression.new((0...5).to_a, exp_avg.last(5).map { |e| e.last })

        case
          when reg.slope == 0
            status = :ok
          when reg.slope > 0
            status = :good
          when reg.slope < 0
            status = :bad
        end

        {
          application_id: @probe.application_id,
          probe_id: @probe.id,
          series: { exponential: exp_avg },
          status: status,
          problematic_modules: modules
        }
      else
        {
          application_id: @probe.application_id,
          probe_id: @probe.id,
          series: { exponential: exp_avg },
          status: :good,
          problematic_modules: modules
        }
      end
    end
  end
end
