module Analyzers
  class PerformanceAnalyzer
    def initialize(probe_id)
      @probe = Probe.find(probe_id)
    end

    def percentile(values, percentile)
      if values.count > 1
        values_sorted = values.sort
        k = (percentile * (values_sorted.length - 1) + 1).floor - 1
        f = (percentile * (values_sorted.length - 1) + 1).modulo(1)

        return values_sorted[k] + (f * (values_sorted[k + 1] - values_sorted[k]))
      else
        values.first
      end
    end

    def calculate
      results = {}
      pages = @probe.probe_infos.group_by { |p| p.params['page'] }

      pages.map do |k,v|
        load_times = v.map do |p|
          {
            params: p.params,
            page: p.params['page'],
            datetime: DateTime.parse(p.params['datetime'])
          }
        end
        grouped = load_times.group_by_minute { |p| p[:datetime] }
        grouped = grouped.map{ |k,v| [ k.utc().to_s(), v ] }.to_h

        start_time = DateTime.now
        start_time = start_time.change(sec: 0)
        start_time -= 3.days

        results[k] = []
        modules = []

        while start_time < DateTime.now
          start_time += 5.minutes
          time_string = start_time.utc().to_s
          if grouped[time_string]
            load_time = percentile(grouped[time_string].map { |p| p[:params]['timing']['loadEventEnd'].to_f - p[:params]['timing']['navigationStart'].to_f }, 0.95)
            resources = grouped[time_string].first[:params]['resources'].map { |r| [ r['name'], r['responseEnd'].to_f - r['startTime'].to_f ] }
            modules << resources.sort_by { |p| p.last }.last(5)
          else
            load_time = 0
          end

          results[k] << [ start_time, load_time ]
        end
      end

      {
        application_id: @probe.application_id,
        probe_id: @probe.id,
        series: results,
        status: results.inject(false) { |s,q| s || (q.last.last.last > 2000) } ? :bad : :good,
        problematic_modules: [] #modules.inject(modules.first) { |s,c| s & c }
      }
    end
  end
end
